# ohmyzsh

## Table Of Contents

* [About](#about)
* [License](#license)
* [Author](#author)

## About

> Role that installs ohmyzsh

[Back to table of contents](#table-of-contents)

## License

license (MIT)

[Back to table of contents](#table-of-contents)

## Author

[Brad Stinson](https://www.bradthebuilder.me)

[Back to table of contents](#table-of-contents)
